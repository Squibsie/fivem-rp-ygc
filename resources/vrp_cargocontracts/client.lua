local jobactive = false
local jobBlip = false
local hasJobPerm = false

function cargo_DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function deliveryReached(pay)
  local ped = GetPlayerPed(-1)
  local vehicle = GetEntityModel(GetVehiclePedIsIn(ped, false))

  Citizen.Trace(vehicle)
  if (vehicle == 2053223216) then
    TriggerServerEvent("DeliveryComplete", (pay*1.5))
    cargo_DisplayHelpText("Delivery Complete! Vehicle Bonus applied!")
  elseif (vehicle == 904750859 or vehicle == -1050465301) then
		TriggerServerEvent("DeliveryComplete", pay)
    cargo_DisplayHelpText("Delivery Complete!")
	elseif (vehicle == 2112052861) then
		TriggerServerEvent("DeliveryComplete", (pay*2))
    cargo_DisplayHelpText("Delivery Complete! Vehicle Bonus applied!")
	else
    cargo_DisplayHelpText("Wrong Vehicle! You must drive a DOT approved truck!")
  end
end

function startCargoRun()
  job = math.random(27)
  jobactive = true
	  if job == 1 then
	    jobBlip = AddBlipForCoord(-596.8662109375,5296.1801757813,69.985748291016)
	    SetBlipRoute(jobBlip, 1)
	    cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
	    Citizen.CreateThread(function()
	      while true do
	        local pos = GetEntityCoords(GetPlayerPed(-1), true)
	        if (Vdist2(pos.x, pos.y, pos.z, -596.8662109375,5296.1801757813,69.985748291016) <10) then
	          if jobactive == true then
	            deliveryReached(50)
	            RemoveBlip(jobBlip)
	            jobactive = false
	          end
	        end
	        Citizen.Wait(0)
	      end
	    end)
	  elseif job == 2 then
	    jobBlip = AddBlipForCoord(-1084.8920898438,4924.5092773438,213.91603088379)
	    SetBlipRoute(jobBlip, 1)
	    cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
	    Citizen.CreateThread(function()
	      while true do
	        local pos = GetEntityCoords(GetPlayerPed(-1), true)
	        if (Vdist2(pos.x, pos.y, pos.z, -1084.8920898438,4924.5092773438,213.91603088379) <10) then
	          if jobactive == true then
	            deliveryReached(75)
	            RemoveBlip(jobBlip)
	            jobactive = false
	          end
	        end
	        Citizen.Wait(0)
	      end
	    end)
	  elseif job == 3 then
	    jobBlip = AddBlipForCoord(1697.1849365234,4834.4008789063,41.712749481201)
	    SetBlipRoute(jobBlip, 1)
	    cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
	    Citizen.CreateThread(function()
	      while true do
	        local pos = GetEntityCoords(GetPlayerPed(-1), true)
	        if (Vdist2(pos.x, pos.y, pos.z, 1697.1849365234,4834.4008789063,41.712749481201) <10) then
	          if jobactive == true then
	            deliveryReached(75)
	            RemoveBlip(jobBlip)
	            jobactive = false
	          end
	        end
	        Citizen.Wait(0)
	      end
	    end)
	  elseif job == 4 then
	    jobBlip = AddBlipForCoord(3805.8149414063,4451.5576171875,3.9463710784912)
	    SetBlipRoute(jobBlip, 1)
	    cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
	    Citizen.CreateThread(function()
	      while true do
	        local pos = GetEntityCoords(GetPlayerPed(-1), true)
	        if (Vdist2(pos.x, pos.y, pos.z, 3805.8149414063,4451.5576171875,3.9463710784912) <10) then
	          if jobactive == true then
	            deliveryReached(100)
	            RemoveBlip(jobBlip)
	            jobactive = false
	          end
	        end
	        Citizen.Wait(0)
	      end
	    end)
	  elseif job == 5 then
	    jobBlip = AddBlipForCoord(1696.9912109375,6401.5615234375,31.970252990723)
	    SetBlipRoute(jobBlip, 1)
	    cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
	    Citizen.CreateThread(function()
	      while true do
	        local pos = GetEntityCoords(GetPlayerPed(-1), true)
	        if (Vdist2(pos.x, pos.y, pos.z, 1696.9912109375,6401.5615234375,31.970252990723) <10) then
	          if jobactive == true then
	            deliveryReached(50)
	            RemoveBlip(jobBlip)
	            jobactive = false
	          end
	        end
	        Citizen.Wait(0)
	      end
	    end)
	  elseif job == 6 then
	    jobBlip = AddBlipForCoord(2109.8137207031,4768.896484375,40.958301544189)
	    SetBlipRoute(jobBlip, 1)
	    cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
	    Citizen.CreateThread(function()
	      while true do
	        local pos = GetEntityCoords(GetPlayerPed(-1), true)
	        if (Vdist2(pos.x, pos.y, pos.z, 2109.8137207031,4768.896484375,40.958301544189) <10) then
	          if jobactive == true then
	            deliveryReached(75)
	            RemoveBlip(jobBlip)
	            jobactive = false
	          end
	        end
	        Citizen.Wait(0)
	      end
	    end)
	  elseif job == 7 then
	    jobBlip = AddBlipForCoord(1699.314453125,4939.1318359375,41.848876953125)
	    SetBlipRoute(jobBlip, 1)
	    cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			TriggerServerEvent("cargocontracts:helpnotification")
	    Citizen.CreateThread(function()
	      while true do
	        local pos = GetEntityCoords(GetPlayerPed(-1), true)
	        if (Vdist2(pos.x, pos.y, pos.z, 1699.314453125,4939.1318359375,41.848876953125) <10) then
	          if jobactive == true then
	            deliveryReached(75)
	            RemoveBlip(jobBlip)
	            jobactive = false
	          end
	        end
	        Citizen.Wait(0)
	      end
	    end)
		elseif job == 8 then
			jobBlip = AddBlipForCoord(1315.5460205078,4358.4282226563,40.369743347168)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 1315.5460205078,4358.4282226563,40.369743347168) <10) then
						if jobactive == true then
							deliveryReached(75)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 9 then
			jobBlip = AddBlipForCoord(726.11895751953,4172.6831054688,40.17866897583)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 726.11895751953,4172.6831054688,40.17866897583) <10) then
						if jobactive == true then
							deliveryReached(100)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 10 then
			jobBlip = AddBlipForCoord(2710.5451660156,4336.5297851563,45.321033477783)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 2710.5451660156,4336.5297851563,45.321033477783) <10) then
						if jobactive == true then
							deliveryReached(75)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 11 then
			jobBlip = AddBlipForCoord(2920.2868652344,4465.9428710938,47.525978088379)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 2920.2868652344,4465.9428710938,47.525978088379) <10) then
						if jobactive == true then
							deliveryReached(75)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 12 then
			jobBlip = AddBlipForCoord(2936.0932617188,4638.2514648438,48.013164520264)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 2936.0932617188,4638.2514648438,48.013164520264) <10) then
						if jobactive == true then
							deliveryReached(75)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 13 then
			jobBlip = AddBlipForCoord(2909.4812011719,4361.0874023438,49.793655395508)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 2909.4812011719,4361.0874023438,49.793655395508) <10) then
						if jobactive == true then
							deliveryReached(75)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 14 then
			jobBlip = AddBlipForCoord(3532.4008789063,3672.3532714844,33.350158691406)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 3532.4008789063,3672.3532714844,33.350158691406) <10) then
						if jobactive == true then
							deliveryReached(150)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 15 then
			jobBlip = AddBlipForCoord(2199.6198730469,5615.0,53.262615203857)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 2199.6198730469,5615.0,53.262615203857) <10) then
						if jobactive == true then
							deliveryReached(100)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 16 then
			jobBlip = AddBlipForCoord(1087.7961425781,6504.255859375,20.527002334595)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 1087.7961425781,6504.255859375,20.527002334595) <10) then
						if jobactive == true then
							deliveryReached(50)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 17 then
			jobBlip = AddBlipForCoord(173.92192077637,6625.78125,31.170988082886)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 173.92192077637,6625.78125,31.170988082886) <10) then
						if jobactive == true then
							deliveryReached(25)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 18 then
			jobBlip = AddBlipForCoord(-235.8240814209,6307.5634765625,30.760725021362)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, -235.8240814209,6307.5634765625,30.760725021362) <10) then
						if jobactive == true then
							deliveryReached(25)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 19 then
			jobBlip = AddBlipForCoord(-680.67413330078,5820.7080078125,16.800031661987)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, -680.67413330078,5820.7080078125,16.800031661987) <10) then
						if jobactive == true then
							deliveryReached(25)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 20 then
			jobBlip = AddBlipForCoord(-770.79022216797,5583.97265625,32.954597473145)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, -770.79022216797,5583.97265625,32.954597473145) <10) then
						if jobactive == true then
							deliveryReached(25)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 21 then
			jobBlip = AddBlipForCoord(-1493.6101074219,4981.2329101563,62.596446990967)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, -1493.6101074219,4981.2329101563,62.596446990967) <10) then
						if jobactive == true then
							deliveryReached(50)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 22 then
			jobBlip = AddBlipForCoord(-2203.818359375,4265.8002929688,47.748523712158)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, -2203.818359375,4265.8002929688,47.748523712158) <10) then
						if jobactive == true then
							deliveryReached(75)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 23 then
			jobBlip = AddBlipForCoord(49.424980163574,3728.0734863282,39.339721679688)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 49.424980163574,3728.0734863282,39.339721679688) <10) then
						if jobactive == true then
							deliveryReached(100)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 24 then
			jobBlip = AddBlipForCoord(2504.5227050782,4102.7265625,38.054084777832)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 2504.5227050782,4102.7265625,38.054084777832) <10) then
						if jobactive == true then
							deliveryReached(100)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 25 then
			jobBlip = AddBlipForCoord(2514.9077148438,4221.2109375,39.666973114014)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 2514.9077148438,4221.2109375,39.666973114014) <10) then
						if jobactive == true then
							deliveryReached(100)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 26 then
			jobBlip = AddBlipForCoord(2713.6306152344,4135.849609375,43.714698791504)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 2713.6306152344,4135.849609375,43.714698791504) <10) then
						if jobactive == true then
							deliveryReached(100)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		elseif job == 27 then
			jobBlip = AddBlipForCoord(2752.6762695312,4417.318359375,48.309711456298)
			SetBlipRoute(jobBlip, 1)
			cargo_DisplayHelpText("Delivery Active! Head to the drop-off blip!")
			Citizen.CreateThread(function()
				while true do
					local pos = GetEntityCoords(GetPlayerPed(-1), true)
					if (Vdist2(pos.x, pos.y, pos.z, 2752.6762695312,4417.318359375,48.309711456298) <10) then
						if jobactive == true then
							deliveryReached(100)
							RemoveBlip(jobBlip)
							jobactive = false
						end
					end
					Citizen.Wait(0)
				end
			end)
		end
end

RegisterNetEvent("cargo:cancelcargo")
AddEventHandler("cargo:cancelcargo",function()
	if (jobactive == true) then
		RemoveBlip(jobBlip)
		jobactive = false
		cargo_DisplayHelpText("You have cancelled your cargo delivery.")
	end
end)

-- Init
local blip = AddBlipForCoord(-79.704544067383,6501.8344726563,31.490900039673)
SetBlipSprite(blip, 67)
SetBlipScale(blip, 0.8)
SetBlipAsShortRange(blip, true)
BeginTextCommandSetBlipName("STRING")
AddTextComponentString("Trucking Depot")
EndTextCommandSetBlipName(blip)
local inArea = false
Citizen.CreateThread(function()
  while true do
    local pos = GetEntityCoords(GetPlayerPed(-1), true)
    if (Vdist(pos.x, pos.y, pos.z, -79.704544067383,6501.8344726563,31.490900039673) <2) then
      if (inArea == false) then
        cargo_DisplayHelpText("Press ~INPUT_CONTEXT~ to get a Truck Cargo Contract.")
      end
      inArea = true
      if(IsControlJustReleased(1, 51)) then
				if (jobactive == false) then
        	startCargoRun()
				end
      end
    end
    if (Vdist(pos.x, pos.y, pos.z, -79.704544067383,6501.8344726563,31.490900039673) >3) then
      inArea = false
    end
    Citizen.Wait(0)
  end
end)
