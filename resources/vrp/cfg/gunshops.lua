
local cfg = {}
-- list of weapons for sale
-- for the native name, see https://wiki.fivem.net/wiki/Weapons (not all of them will work, look at client/player_state.lua for the real weapon list)
-- create groups like for the garage config
-- [native_weapon_name] = {display_name,body_price,ammo_price,description}
-- ammo_price can be < 1, total price will be rounded

-- _config: blipid, blipcolor, permissions (optional, only users with the permission will have access to the shop)

cfg.gunshop_types = {
["Paleto"] = {
    _config = {blipid=110,blipcolor=1},
    ["WEAPON_BAT"] = {"Bat",50,0,""},
    ["WEAPON_HAMMER"] = {"Hammer",50,0,""},
    ["WEAPON_KNUCKLE"] = {"Knuckle Duster",100,0,""},
    ["WEAPON_KNIFE"] = {"Knife",50,0,""},
    ["WEAPON_SWITCHBLADE"] = {"Switchblade Knife",70,0,""},
    ["WEAPON_MARKSMANPISTOL"] = {"Marksman Pistol",2000,2,""},
    ["WEAPON_SNSPISTOL"] = {"Pistol",3000,2,""},
    ["WEAPON_VINTAGEPISTOL"] = {"Vintage Pistol",3000,2,""},
    ["WEAPON_PISTOL"] = {"Pistol",4000,2,""},
    ["WEAPON_COMBATPISTOL"] = {"Combat Pistol",6500,2,""},
    ["WEAPON_HEAVYPISTOL"] = {"Heavy Pistol",8000,2,""},
    ["WEAPON_REVOLVER"] = {"Revolver",9500,4,""},
    ["WEAPON_DOUBLEBARRELSHOTGUN"] = {"Double Barrelled Shotgun",15000,2,""},
    ["WEAPON_PUMPSHOTGUN"] = {"Revolver",17000,2,""},
    ["WEAPON_STUNGUN"] = {"Stun Gun",2500,0,""},
  },
  ["policeloadout"] = {
    _config = {blipid=110,blipcolor=74, permissions = {"police.loadshop"}},
    ["WEAPON_PUMPSHOTGUN"] = {"Pump Shotgun",0,0,""},
	  ["WEAPON_FLASHLIGHT"] = {"Flashlight",0,0,""},
    ["WEAPON_NIGHTSTICK"] = {"Nighstick",0,0,""},
	  ["WEAPON_STUNGUN"] = {"Tazer",0,0,""},
    ["WEAPON_COMBATPISTOL"] = {"Combat Pistol",0,0,""},
    ["WEAPON_CARBINERIFLE"] = {"Carbine Rifle",0,0,""},
    ["WEAPON_FIREEXTINGUISHER"] = {"Fire Extinguisher",0,0,""}
  },
  ["Security Weapon Store"] = {
    _config = {blipid=110,blipcolor=74, permissions = {"security.gunshop"}},
    ["WEAPON_PISTOL"] = {"Pistol",50,0,""},
	  ["WEAPON_COMBATPISTOL"] = {"Combat Pistol",200,0,""},
    ["WEAPON_FLASHLIGHT"] = {"Flashlight",0,0,""},
    ["WEAPON_NIGHTSTICK"] = {"Nighstick",0,0,""},
	  ["WEAPON_STUNGUN"] = {"Taser",0,0,""},
    ["WEAPON_PUMPSHOTGUN"] = {"Shotgun",250,0,""}
  },
  ["EMS Equipment Locker"] = {
    _config = {blipid=110,blipcolor=74, permissions = {"ems.gunshop"}},
    ["WEAPON_FIREEXTINGUISHER"] = {"Fire Extinguisher",50,0,""},
	  ["WEAPON_STUNGUN"] = {"TASER",200,0,""}
  },
}
-- list of gunshops positions

cfg.gunshops = {
  {"Paleto", -331.50210571289,6082.5063476563,31.454769134521}, -- Paleto
  {"policeloadout", -448.62390136719,6008.6098632813,31.716371536255},
  {"Security Weapon Store", -101.104637146,6464.125,31.626726150513},
  {"Security Weapon Store", 2904.2856445313,4482.6206054688,48.100006103516},
  {"EMS Equipment Locker", -370.23425292969,6100.4814453125,31.493068695068}
}

return cfg
