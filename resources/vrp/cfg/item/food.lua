-- define some basic inventory items

local items = {}

local function play_eat(player)
  local seq = {
    {"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

local function play_drink(player)
  local seq = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

-- gen food choices as genfunc
-- idname
-- ftype: eat or drink
-- vary_hunger
-- vary_thirst
local function gen(ftype, vary_hunger, vary_thirst)
  local fgen = function(args)
    local idname = args[1]
    local choices = {}
    local act = "Unknown"
    if ftype == "eat" then act = "Eat" elseif ftype == "drink" then act = "Drink" end
    local name = vRP.getItemName(idname)

    choices[act] = {function(player,choice)
      local user_id = vRP.getUserId(player)
      if user_id ~= nil then
        if vRP.tryGetInventoryItem(user_id,idname,1,false) then
          if vary_hunger ~= 0 then vRP.varyHunger(user_id,vary_hunger) end
          if vary_thirst ~= 0 then vRP.varyThirst(user_id,vary_thirst) end

          if ftype == "drink" then
            vRPclient.notify(player,{"~b~ Drinking "..name.."."})
            play_drink(player)
          elseif ftype == "eat" then
            vRPclient.notify(player,{"~o~ Eating "..name.."."})
            play_eat(player)
          end

          vRP.closeMenu(player)
        end
      end
    end}

    return choices
  end

  return fgen
end

-- DRINKS --

items["water"] = {"Water","A bottle of water.", gen("drink",0,-25),0.5}
items["milk"] = {"Milk","", gen("drink",-0.5,-5),0.5}
items["bcoffee"] = {"Black Coffee","", gen("drink",1,-10),0.2}
items["americano"] = {"Americano","", gen("drink",0.5,-10),0.2}
items["latte"] = {"Latte","", gen("drink",0.25,-8),0.2}
items["espresso"] = {"Espresso","A short, strong drink.", gen("drink",0.1,-6),0.1}
items["tea"] = {"Tea","", gen("drink",0,-10),0.2}
items["icetea"] = {"Ice-Tea","", gen("drink",0,-20), 0.5}
items["orangejuice"] = {"Orange Juice","", gen("drink",0,-25),0.5}
items["forangejuice"] = {"Fresh Orange Juice","Freshly squeezed on site!", gen("drink",0,-30),0.5}
items["smoothie"] = {"Fruit Smoothie","Made on site, helps keep away hunger also!", gen("drink",1,-30),0.5}
items["cocacola"] = {"Coca Cola","", gen("drink",0,-35),0.3}
items["redbull"] = {"Red Bull","", gen("drink",0,-40),0.3}
items["lemonade"] = {"Lemonade","", gen("drink",0,-45),0.3}
--alcohol
items["floorslite"] = {"Floors Lite","", gen("drink",2,-30),0.5}
items["vodka"] = {"Vodka","", gen("drink",5,-15),0.5}
items["whiskey"] = {"Whiskey","", gen("drink",5,-15),0.5}

--FOOD

-- create Breed item
items["bread"] = {"Bread","", gen("eat",-10,2),0.5}
items["donut"] = {"Donut","A Cheap Donut", gen("eat",-15,5),0.2}
items["sandwich"] = {"Sandwich","A tasty snack.", gen("eat",-25,2),0.5}
items["fchicken"] = {"Fried Chicken","A bucket of fried chicken.", gen("eat",-45,5),0.8}
items["burrito"] = {"Burrito","A nicely wrapped mexican style burrito.", gen("eat",-40,0),0.5}
items["tacos"] = {"Tacos","Hardshell, spicy tacos.", gen("eat",-50, 10),0.8}
items["kebab"] = {"Kebab","", gen("eat",-45,0),0.85}
items["pdonut"] = {"Premium Donut","", gen("eat",-25,0),0.5}
items["catfish"] = {"Catfish","", gen("eat",10,15),0.3}
items["bass"] = {"Bass","", gen("eat",10,15),0.3}
items["pdonut"] = {"Fresh Donut","A donut freshly baked this morning!", gen("eat",-20,2),0.2}
items["fries"] = {"Potato Fries","Hot potato fries.", gen("eat",-25,5),0.6}

-- Food truck items
items["fttacos"] = {"Handmade Tacos","Hardshell, Handmade tacos.", gen("eat",-70, 5),1}
items["ftburrito"] = {"Handmade Burrito","A large burrito in a soft white wrap!", gen("eat",-60, 2),0.8}
items["ftburrito2"] = {"Handmade Spicy Burrito","A super spicy burrito.", gen("eat",-60, 15),1}
items["ftdbits"] = {"Donut Bits","Little mini donut 'bits' rolled in sugar.", gen("eat",-20, 0),0.2}
items["ftfries"] = {"Fries","Piping hot fries", gen("eat",-30, 2),0.3}

--fruits
items["orange"] = {"Orange","A single, locally grown orange", gen("eat",-5,-2.5),0.2}
items["pear"] = {"Pear","A single, locally grown pear", gen("eat",-6,-2.5),0.2}
items["watermelon"] = {"Watermelon","A large fresh watermelon", gen("eat",-10,-6),0.5}

return items
