
local items = {}

local function play_drink(player)
  local seq = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

local phanadryl_choices = {}
phanadryl_choices["Take"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"phanadryl",1) then
      vRPclient.varyHealth(player,{10})
      vRP.varyThirst(user_id, 5)
      vRPclient.notify(player,{"~g~ Taking pills."})
      play_drink(player)
      vRP.closeMenu(player)
    end
  end
end}

local thaludimol_choices = {}
thaludimol_choices["Take"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"thaludimol",1) then
      vRPclient.varyHealth(player,{50})
      vRPclient.notify(player,{"~g~ Taking pills."})
      play_drink(player)
      vRP.closeMenu(player)
    end
  end
end}

local function play_smoke(player)
  local seq2 = {
    {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
    {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
    {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
  }

  vRPclient.playAnim(player,{true,seq2,false})
end

local smoke_choices = {}
smoke_choices["Take"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"cannabis",1) then
	  vRP.varyHunger(user_id,(20))
      vRPclient.notify(player,{"~g~ smoking weed."})
      play_smoke(player)
      vRP.closeMenu(player)
      vRPclient.varyHealth(player,{5})
    end
  end
end}

local function play_smell(player)
  local seq3 = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq3,false})
end

local smell_choices = {}
smell_choices["Take"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"cocaine",1) then
	  vRP.varyThirst(user_id,(20))
      vRPclient.notify(player,{"~g~ snorting cocaine."})
      play_smell(player)
      vRP.closeMenu(player)
    end
  end
end}

local function play_lsd(player)
  local seq4 = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq4,false})
end

local meth_choices = {}
meth_choices["Take"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"meth",1) then
	  vRP.varyThirst(user_id,(20))
      vRPclient.notify(player,{"~g~ Taking meth"})
      play_lsd(player)
      vRP.closeMenu(player)
    end
  end
end}

items["phanadryl"] = {"Phanadryl","An over the counter high strength pain medication. Take with a drink!",function(args) return phanadryl_choices end,0.1}
items["thaludimol"] = {"Thaludimol","A controlled high strength painkiller.",function(args) return thaludimol_choices end,0.1}
items["cannabis"] = {"Cannabis","Cannabis prepared ready for smoking",function(args) return smoke_choices end,0.10}
items["cocaine"] = {"Cocaine","A deal bag of cocaine",function(args) return smell_choices end,0.1}
items["meth"] = {"Meth","Processed Meth",function(args) return meth_choices end,0.1}
items["Presents"] = {"Presents","Given to Children."}

return items
