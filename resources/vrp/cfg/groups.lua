
local cfg = {}

-- define each group with a set of permissions
-- _config property:
--- gtype (optional): used to have only one group with the same gtype per player (example: a job gtype to only have one job)
--- onspawn (optional): function(player) (called when the player spawn with the group)
--- onjoin (optional): function(player) (called when the player join the group)
--- onleave (optional): function(player) (called when the player leave the group)
--- (you have direct access to vRP and vRPclient, the tunnel to client, in the config callbacks)

cfg.groups = {
  ["superadmin"] = {
    _config = {onspawn = function(player) vRPclient.notify(player,{"You are superadmin."}) end},
    "player.group.add",
    "player.group.remove",
    "player.givemoney",
    "player.giveitem"
  },
  ["admin"] = {
    "firescript",
    "admin.tickets",
    "admin.announce",
	"admin.menu",
	"admin.easy_unjail",
	"admin.spikes",
  "admin.revive",
	-- "admin.godmode",
	"admin.spawnveh",
	"admin.deleteveh",
	"player.blips",
	"player.tptowaypoint",
    "player.list",
    "player.whitelist",
    "player.unwhitelist",
    "player.kick",
    "player.ban",
    "player.unban",
    "player.noclip",
    "player.custom_emote",
    "player.custom_sound",
    "player.display_custom",
    "player.group.add",
    "player.group.remove",
    "player.coords",
    "player.tptome",
    "player.tpto"
  },
     -- ["god"] = {
    -- "admin.god" -- reset survivals/health periodically
  -- },
  ["recruiter"] = {
    "player.list",
	"player.group.add",
    "player.group.remove"
    },
  ["mod"] = {
	"admin.tickets",
  "firescript",
    "admin.announce",
    "player.list",
    "player.kick",
    "player.coords",
    "player.tptome",
    "player.tpto"
  },
  -- the group user is auto added to all logged players
  ["user"] = {
    "player.phone",
    "player.calladmin",
	"player.fix_haircut",
	"player.check",
	--"mugger.mug",
  "police.askid",
  "police.store_weapons",
	"player.skip_coma",
	"player.store_money",
	"player.check",
	"player.loot",
	"player.player_menu",
	"player.userlist",
  "police.seizable",	-- can be seized
	"user.paycheck",
  "vehicle.repair",
  "illegal.launderer"
  },
  --jobs
  ["Unemployed"] = {
    _config = { gtype = "job",
  onspawn = function(player) vRPclient.notify(player,{"You are Unemployed, go to Job Selector in the city hall/sheriff's station."}) end
  },
  "user.paycheck"
  },
  ["Taxi Driver"] = {
    _config = { gtype = "job",
  onspawn = function(player) vRPclient.notify(player,{"You are a Taxi Driver, Players can ring you for a ride! Make sure to charge them a reasonable rate after the ride!"}) end
  },
  "taxi.service",
  "taxi.vehicle",
  "taxi.paycheck",
  "job.taxable.low",
  "-user.paycheck"
  },
  ["Mechanic"] = {
    _config = { gtype = "job",
  onspawn = function(player) vRPclient.notify(player,{"You are Mechanic. Players can call you for roadside assistance."}) end
  },
  "repair.service",
  "repair.vehicle",
  "repair.market",
  "repair.paycheck",
  "job.taxable.low",
  "-user.paycheck"
  },
  ["Truck Driver"] = {
    _config = { gtype = "job",
  onspawn = function(player) vRPclient.notify(player,{"You are a truck driver, drive your truck and deliver items from the depot."}) end
  },
  "trucker.vehicle",
  "trucker.job",
  "trucker.paycheck",
  "job.taxable.low",
  "-user.paycheck"
  },
  ["Food Truck Driver"] = {
    _config = { gtype = "job",
  onspawn = function(player) vRPclient.notify(player,{"You are Food truck driver, produce food items and sell them to players from your food truck."}) end
  },
  "ftd.vehicle",
  "ftd.paycheck",
  "job.taxable.low",
  "ftd.announce",
  "ftd.kitchen",
  "ftd.market",
  "-user.paycheck"
  },
  -- #################
  -- #Police/EMS Jobs#
  -- #################
  ["Sheriffs Deputy"] = {
    _config = {
      gtype = "job",
      onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{true}) end
    },
    "sheriff.cloakroom",
    "police.pc",
    --"police.handcuff",
    "police.putinveh",
    "police.getoutveh",
	  "police.drag",
	  "police.easy_cuff",
	  "police.easy_fine",
  	"police.easy_jail",
  	"police.easy_unjail",
  	"police.spikes",
  	"police.menu",
    "police.check",
  	"toggle.service",
  	"police.freeze",
    "police.service",
    "police.seize.weapons",
    "police.seize.items",
    --"police.jail",
    --"police.fine",
    "police.announce",
   -- "-police.store_weapons",
    "-police.seizable",	-- negative permission, police can't seize itself, even if another group add the permission
  	"police.vehicle",
  	"police.loadshop",
  	"cop.whitelisted",
  	"police.market",
  	--"player.list",
  	"deputy.paycheck",
    "job.taxable.mid",
  	"police.mission",

  	"police.menu_interaction"
  },
  ["Sheriff"] = {
    _config = {
      gtype = "job",
      onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{true}) end
    },
    "sheriff.cloakroom",
    "police.pc",
    --"police.handcuff",
    "police.putinveh",
    "police.getoutveh",
    "police.drag",
    "police.easy_cuff",
    "police.easy_fine",
    "police.easy_jail",
    "police.easy_unjail",
    "police.spikes",
    "police.menu",
    "police.check",
    "toggle.service",
    "police.freeze",
    "police.service",
    "police.seize.weapons",
    "police.seize.items",
    --"police.jail",
    --"police.fine",
    "police.announce",
   -- "-police.store_weapons",
    "-police.seizable",	-- negative permission, police can't seize itself, even if another group add the permission
    "police.vehicle",
    "police.loadshop",
    "cop.whitelisted",
    "emergency.market",
    "emergency.revive",
    --"player.list",
    "sheriff.paycheck",
    "job.taxable.high",
    "police.menu_interaction"
  },
  ["PBFD"] = {
    _config = { gtype = "job",
  onspawn = function(player) vRPclient.notify(player,{"You are a EMS Paramedic."}) end
  },
    "emergency.revive",
  --"police.wanted",
    "emergency.service",
    "emscheck.revive",
    "emergency.cloakroom",
    "emergency.vehicle",
    "emergency.market",
    "ems.whitelisted",
    "ems.loadshop",
    "ems.gunshop",
  --"player.list",
    "police.menu_interaction",
    "emsMedic.paycheck",
    "job.taxable.mid",
  },
  ["EMS Chief"] = {
    _config = { gtype = "job",
  onspawn = function(player) vRPclient.notify(player,{"You are the EMS Chief."}) end
  },
    "emergency.revive",
  --"police.wanted",
    "emergency.service",
    "emscheck.revive",
    "emergency.cloakroom",
    "emergency.vehicle",
    "emergency.market",
    "ems.whitelisted",
    "ems.loadshop",
    "ems.gunshop",
  --"player.list",
    "police.menu_interaction",
    "emsChief.paycheck",
    "job.taxable.high"
  },
  --###############
  --#Security Jobs#
  --###############
  ["security"] = {
    _config = {
      onspawn = function(player) vRPclient.notify(player,{"You are a state licensed security guard. You can work security jobs now."}) end,
      onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{true}) end
  },
    "security.cloakroom",
    "security.vehicle",
    "police.market",
    "security.whitelisted",
    "security.gunshop",
    "police.menu_interaction",
    "police.menu",
    "police.check",
    "police.putinveh",
    "police.getoutveh",
    "police.drag",
    "police.easy_cuff",
    "-illegal.launderer"
  },
--[[  ["Paleto Prison Guard"] = {
    _config = { gtype = "job",
  onspawn = function(player) vRPclient.notify(player,{"You are a Paleto Prison Guard. You need to look after the prison."}) end
  },
    "prison.vehicle",
    "police.seize.weapons",
    "police.seize.items",
    "security.paycheck",
    "job.taxable.low",
    "police.easy_jail",
    "police.easy_unjail",
    "toggle.service"
  },]]
  ["Bank Security"] = {
    _config = { gtype = "job",
  onspawn = function(player) vRPclient.notify(player,{"You are a Bank Security Guard. Secure the bank!"}) end
  },
    "bank.vehicle",
    "police.seize.weapons",
    "police.seize.items",
    "security.paycheck",
    "job.taxable.low",
    "toggle.service"
  },
  ["GSFR Security Officer"] = {
    _config = { gtype = "job",
  onspawn = function(player) vRPclient.notify(player,{"You are a Bobcat Response Officer, respond and patrol for security incidents."}) end
  },
    "bobcat.vehicle",
    "police.seize.weapons",
    "police.seize.items",
    "security.paycheck",
    "job.taxable.low",
    "security.service",
    "toggle.service"
  },
  --GANGS
  ["piratesmc"] = {
    _config = {
      onspawn = function(player) vRPclient.notify(player,{"You are a member of Pirates MC!"}) end,
  },
    "piratesmc.cloakroom",
    "piratesmc.vehicle",
    "gang.whitelisted",
    "police.menu_interaction",
    "gang.drugs"
  },

  -- ##OLD JOBS
  ["Fisher"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"You are a Fisherman."}) end
	},
	"mission.delivery.fish",
    "fisher.service",
	"fisher.vehicle"
    },
  ["Bankdriver"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"You are a Bank Driver. Salary: $2000."}) end
	},
	"mission.bankdriver.moneybank",
	"mission.bankdriver.moneybank2",
	"bankdriver.vehicle",
	"bankdriver.paycheck",
	"bankdriver.money"
  },
  ["cop"] = {
    "cop.whitelisted"
  },
  ["mafia"] = {
	"mafia.whitelisted"
  },
  ["ems"] = {
    "ems.whitelisted"
  },
  ["moderator"] = {
    "president.whitelisted"
  }
}

-- groups are added dynamically using the API or the menu, but you can add group when an user join here
cfg.users = {
  [1] = { -- give superadmin and admin group to the first created user on the database
    "superadmin",
    "admin",
	"recruiter"
  }
}

-- group selectors
-- _config
--- x,y,z, blipid, blipcolor, permissions (optional)

cfg.selectors = {
  ["Job Selector"] = {
    _config = {x = -448.89395141602, y = 6018.2626953125, z = 31.716371536255, blipid = 351, blipcolor = 47},
    "Taxi Driver",
    "Mechanic",
	  "Truck Driver",
	  "Food Truck Driver",
    "Unemployed"
  },
  ["Sheriff Jobs"] = { -- Paleto Bay
    _config = {x = -449.00927734375, y = 6017.1953125, z = 31.716377258301, blipid = 351, blipcolor = 38, permissions = {"cop.whitelisted"} },
	"Sheriffs Deputy",
	"Unemployed"
    },
  ["PBFD Jobs"] = { -- Spawn Hospital
    _config = {x =  -373.99206542969, y = 6103.865234375, z = 31.493085861206, blipid = 351, blipcolor = 3, permissions = {"ems.whitelisted"} },
    "PBFD",
	  "Unemployed"
  },
  ["Bank Security Jobs"] = {
    _config = {x =  -99.637420654297, y = 6461.4370117188, z = 31.626703262329, blipid = 351, blipcolor = 10, permissions = {"security.whitelisted"} },
    "Bank Security",
    "Unemployed"
  },
  ["Bobcat Response Security Jobs"] = {
    _config = {x =  2911.2094726563, y = 4492.5512695313, z = 48.105892181396, blipid = 351, blipcolor = 10, permissions = {"security.whitelisted"} },
    "Bobcat Response Officer (Security)",
    "Unemployed"
  }
}

return cfg
