local cfg = {}

-- define static item transformers
-- see https://github.com/ImagicTheCat/vRP to understand the item transformer concept/definition

cfg.item_transformers = {
  -- example of harvest item transformer
  {
    name="Food Truck Kitchen", -- menu name
    permissions = {"@cooking.foodprep.3"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=100,
    units_per_minute=10,
    x=-87.296981811523,y=6228.71875,z=31.089887619019,
    radius=3, height=1.5, -- area
    recipes = {
      ["Cook 5 Tacos"] = { -- action name
        description="Cook 5 Tacos. REQUIRES: Taco Shells, Mincemeat, Mexican Beans and Salsa.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["tacoshells"] = 1,
          ["mincemeat"] = 1,
          ["mexbeans"] = 1,
          ["salsa"] = 1
        }, -- items taken per unit
        products={ -- items given per unit
          ["fttacos"] = 5
        }
      },
      ["Cook 5 Burritos."] = { -- action name
        description="REQUIRES: Tortillas, Mincemeat, Mexican Beans and Salsa.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["tortillas"] = 1,
          ["mincemeat"] = 1,
          ["mexbeans"] = 1,
          ["salsa"] = 1
        }, -- items taken per unit
        products={ -- items given per unit
          ["ftburrito"] = 5
        }
      },
      ["Cook 5 Spicy Burritos."] = { -- action name
        description="REQUIRES: Tortillas, Mincemeat, Mexican Beans and Spicy Salsa.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["tortillas"] = 1,
          ["mincemeat"] = 1,
          ["mexbeans"] = 1,
          ["spicysalsa"] = 1
        }, -- items taken per unit
        products={ -- items given per unit
          ["ftburrito2"] = 5
        }
      },
      ["Fry Donut Bits (5 Portions)"] = { -- action name
        description="REQUIRES: Batter and Sugar", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["batter"] = 1,
          ["sugar"] = 1
        }, -- items taken per unit
        products={ -- items given per unit
          ["ftdbits"] = 5
        }
      },
      ["Fry fries"] = { -- action name
        description="REQUIRES: Raw Fries", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["rawfries"] = 1
        }, -- items taken per unit
        products={ -- items given per unit
          ["ftfries"] = 5
        }
      }
    }
  },
  {
    name="Grapeseed Technical School", -- menu name
    r=255,g=125,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=2704.0827636719,y=4330.4897460938,z=45.851997375488,
    radius=7.5, height=1.5, -- area
      recipes = {
        ["Food Prep 101"] = { -- action name
          description="Learn how to prep food to a commercial standard (Food Truck Skill)", -- action description
          in_money=200, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={}, -- items given per unit
          aptitudes={ -- optional
            ["cooking.foodprep"] = 3 -- "group.aptitude" -- One level?
          }
        }
      }
  },
  {
    name="Smithing", -- menu name
    r=255,g=125,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=3726.8708496094,y=4540.6704101563,z=21.399703979492,
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Assemble AK47"] = { -- action name
        description="Construct an AK47", -- action description
        in_money=200, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["akreceiver"] = 1,
          ["akbarrel"] = 1,
          ["aktrigger"] = 1,
          ["akcomponents"] = 1
        }, -- items taken per unit
        products={}, -- items given per unit
        aptitudes={}
      },
      ["Assemble Machine Pistol"] = { -- action name
        description="Construct a Machine Pistol", -- action description
        in_money=200, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["mpbody"] = 1,
          ["mptrigger"] = 1
        }, -- items taken per unit
        products={}, -- items given per unit
        aptitudes={}
      },
      ["Assemble RPG"] = { -- action name
        description="Construct an RPG", -- action description
        in_money=300, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["rpgshell"] = 1,
          ["rpgtube"] = 1,
          ["rpgreceiver"] = 1,
          ["rpgtrigger"] = 1,
          ["explosiveord"] = 1,
        }, -- items taken per unit
        products={}, -- items given per unit
        aptitudes={}
      }
    },
    onstart = function(player,recipe)
      if recipe == "Assemble AK47" then
        Citizen.CreateThread(function()
          vRPclient.notify(player,{"Assembling AK47..."})
          Citizen.Wait(10000)
          vRPclient.giveWeapons(player,{{
          ["WEAPON_ASSAULTRIFLE"] = {ammo=60}
          }, false})
        end)
      elseif recipe == "Assemble Machine Pistol" then
        Citizen.CreateThread(function()
          vRPclient.notify(player,{"Assembling Machine Pistol..."})
          Citizen.Wait(10000)
          vRPclient.giveWeapons(player,{{
          ["WEAPON_MACHINEPISTOL"] = {ammo=60}
          }, false})
        end)
      elseif recipe == "Assemble RPG" then
        Citizen.CreateThread(function()
          vRPclient.notify(player,{"Assembling RPG..."})
          Citizen.Wait(20000)
          vRPclient.giveWeapons(player,{{
          ["WEAPON_RPG"] = {ammo=1}
          }, false})
        end)
      else
        vRPclient.notify(player,{"ERROR: Unable to load recipe. Gunsmithing. Please report on Discord!"})
      end
    end,
    onstep = function(player,recipe) end, -- optional step callback
    onstop = function(player,recipe) end -- optional stop callback
  },
       {
    name="Trash Collector", -- menu name
    permissions = {"mission.collect.trash"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=100000,
    units_per_minute=100000,
    x=805.77130126953,y=-1078.0639648438,z=28.55744934082,
    radius=3, height=1.5, -- area
    recipes = {
      ["Gather Trash"] = { -- action name
        description="Gathering Trash...", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["trash"] = 1
        }
      }
    },
  },
  {
    name="Bank Cash Deposit", -- menu name
    permissions = {"bank.vehicle"},
    r=255,g=125,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=-95.275238037109,y=6474.015625,z=31.419563293457,
    radius=5, height=1.5, -- area
    recipes = {
      ["Deposit Cash Box"] = { -- action name
        description="Deposit Cash Boxes you have collected.", -- action description
        in_money=0, -- money taken per unit
        out_money=150, -- money earned per unit
        reagents={
          ["cashbox"] = 1
        }, -- items taken per unit
        products={}, -- items given per unit
        aptitudes={}
      }
    },
  },
    {
      name="Pickup Cashboxes 1", -- menu name
      permissions = {"bank.vehicle"},
      r=255,g=125,b=0, -- color
      max_units=20,
      units_per_minute=1,
      x=-754.21362304688,y=5579.0087890625,z=36.709636688232,
      radius=5, height=1.5, -- area
      recipes = {
        ["Collect Cash Boxes (x2)"] = { -- action name
          description="Collect 2 cash boxes! Put them in your cash van and collect more until this location runs out!", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={
          }, -- items taken per unit
          products={
            ["cashbox"] = 2
          }, -- items given per unit
          aptitudes={}
        }
      },
    },
    {
      name="Pickup Cashboxes 2", -- menu name
      permissions = {"bank.vehicle"},
      r=255,g=125,b=0, -- color
      max_units=20,
      units_per_minute=1,
      x=174.80598449707,y=6643.0297851563,z=31.573141098022,
      radius=7.5, height=1.5, -- area
      recipes = {
        ["Collect Cash Boxes (x2)"] = { -- action name
          description="Collect 2 cash boxes! Put them in your cash van and collect more until this location runs out!", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={
          }, -- items taken per unit
          products={
            ["cashbox"] = 2
          }, -- items given per unit
          aptitudes={}
        }
      },
    },
    {
      name="Pickup Cashboxes 3", -- menu name
      permissions = {"bank.vehicle"},
      r=255,g=125,b=0, -- color
      max_units=20,
      units_per_minute=1,
      x=1741.6248779297,y=6419.8471679688,z=35.042266845703,
      radius=7.5, height=1.5, -- area
      recipes = {
        ["Collect Cash Boxes (x2)"] = { -- action name
          description="Collect 2 cash boxes! Put them in your cash van and collect more until this location runs out!", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={
          }, -- items taken per unit
          products={
            ["cashbox"] = 2
          }, -- items given per unit
          aptitudes={}
        }
      },
    },
    {
      name="Pickup Cashboxes 4", -- menu name
      permissions = {"bank.vehicle"},
      r=255,g=125,b=0, -- color
      max_units=20,
      units_per_minute=1,
      x=1700.5152587891,y=4816.2153320313,z=41.93529510498,
      radius=7.5, height=1.5, -- area
      recipes = {
        ["Collect Cash Boxes (x2)"] = { -- action name
          description="Collect 2 cash boxes! Put them in your cash van and collect more until this location runs out!", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={
          }, -- items taken per unit
          products={
            ["cashbox"] = 2
          }, -- items given per unit
          aptitudes={}
        }
      },
    },
    {
      name="Pickup Cashboxes 5", -- menu name
      permissions = {"bank.vehicle"},
      r=255,g=125,b=0, -- color
      max_units=20,
      units_per_minute=1,
      x=1794.8665771484,y=4603.6064453125,z=37.682830810547,
      radius=7.5, height=1.5, -- area
      recipes = {
        ["Collect Cash Boxes (x2)"] = { -- action name
          description="Collect 2 cash boxes! Put them in your cash van and collect more until this location runs out!", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={
          }, -- items taken per unit
          products={
            ["cashbox"] = 2
          }, -- items given per unit
          aptitudes={}
        }
      },
    },
    {
      name="Pickup Cashboxes 6", -- menu name
      permissions = {"bank.vehicle"},
      r=255,g=125,b=0, -- color
      max_units=20,
      units_per_minute=1,
      x=1710.5745849609,y=4929.8837890625,z=42.078178405762,
      radius=7.5, height=1.5, -- area
      recipes = {
        ["Collect Cash Boxes (x2)"] = { -- action name
          description="Collect 2 cash boxes! Put them in your cash van and collect more until this location runs out!", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={
          }, -- items taken per unit
          products={
            ["cashbox"] = 2
          }, -- items given per unit
          aptitudes={}
        }
      },
    },
  {
    name="Body training", -- menu name
    r=255,g=125,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=-1202.96252441406,y=-1566.14086914063,z=4.61040639877319,
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Strength"] = { -- action name
        description="Increase your strength.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={}, -- items given per unit
        aptitudes={ -- optional
          ["physical.strength"] = 0.2 -- "group.aptitude", give 1 exp per unit
        }
      }
    }
  }
}

-- define transformers randomly placed on the map
cfg.hidden_transformers = {
  ["Weapon Black Market"] = {
    def = {
      name="Weapon Black Market", -- menu name
      r=0,g=200,b=0, -- color
      max_units=10,
      units_per_minute=1,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["AK Receiver"] = { -- action name
          description="Required to make an AK47.", -- action description
          in_money=500, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["akreceiver"] = 1
          }
        },
        ["AK Barrel"] = { -- action name
          description="Required to make an AK47.", -- action description
          in_money=250, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["akbarrel"] = 1
          }
        },
        ["AK Trigger Body"] = { -- action name
          description="Required to make an AK47.", -- action description
          in_money=250, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["aktrigger"] = 1
          }
        },
        ["AK Components"] = { -- action name
          description="Required to make an AK47.", -- action description
          in_money=500, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["akcomponents"] = 1
          }
        },
        ["Machine Pistol Body"] = { -- action name
          description="Required to make a machine pistol.", -- action description
          in_money=500, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["mpbody"] = 1
          }
        },
        ["Machine Pistol Trigger Body"] = { -- action name
          description="Required to make an machine pistol.", -- action description
          in_money=250, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["mptrigger"] = 1
          }
        },
        ["RPG Shell"] = { -- action name
          description="Required to make an RPG.", -- action description
          in_money=500, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["rpgshell"] = 1
          }
        },
        ["RPG Tube"] = { -- action name
          description="Required to make an RPG.", -- action description
          in_money=500, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["rpgtube"] = 1
          }
        },
        ["RPG Receiver"] = { -- action name
          description="Required to make an RPG.", -- action description
          in_money=1000, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["rpgreceiver"] = 1
          }
        },
        ["RPG Trigger"] = { -- action name
          description="Required to make an RPG.", -- action description
          in_money=500, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["rpgtrigger"] = 1
          }
        },
        ["RPG Explosive Ordnance"] = { -- action name
          description="Required to make an RPG.", -- action description
          in_money=500, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["explosiveord"] = 1
          }
        },
      }
    },
    positions = {
      {432.56042480469,6474.8540039063,28.779050827026},
      {63.297222137451,7049.3056640625,16.670049667358},
      {1363.9250488281,6549.6494140625,14.561595916748},
      {1504.7982177734,6325.44140625,24.082511901855},
      {2427.3352050781,4971.1435546875,45.814697265625},
      {2301.5212402344,4749.13671875,37.03702545166},
      {3827.0524902344,4468.822265625,3.0553138256073},
      {3798.0192871094,4483.3896484375,5.9926867485046},
      {3824.7905273438,4474.6928710938,4.8051381111145},
      {3367.3002929688,5184.0522460938,1.4602422714233},
      {3426.6701660156,5174.6704101563,7.4144787788391}
    }
  },
  ["Launderer"] = {
    def = {
      name="Launderer", -- menu name
      permissions = {"illegal.launderer"},
      r=0,g=200,b=0, -- color
      max_units=100000,
      units_per_minute=100000,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Break Open Cash Box"] = { -- action name
          description="Break into cash boxes 'obtained' from cash vans.", -- action description
          in_money=0, -- money taken per unit
          out_money=1000, -- money earned per unit
          reagents={
            ["cashbox"] = 1
          }, -- items taken per unit
          products={}
        }
      }
    },
    positions = {
      {1538.6826171875,6323.68359375,24.054414749146}
    }
  },
  ["cannabis connect"] = {
    def = {
      name="Cannabis Connection", -- menu name
      permissions = {"gang.drugs"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=500,
      units_per_minute=10,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Collect bales"] = { -- action name
          description="Collect cannabis bale (1KG) from our out of state supplier.", -- action description
          in_money=2000, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["cannabis"] = 10
          }
        }
      }
    },
    positions = {
      {-1366.4187011719,4857.8837890625,137.56372070313},
      {-2326.1479492188,4126.474609375,25.971961975098}
    }
  },
  ["cannabisbuyer1"] = {
    def = {
      name="Cannabis Buyer", -- menu name
      permissions = {"gang.drugs"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=2,
      units_per_minute=0.25,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Sell Cannabis"] = { -- action name
          description="Hey man, you got any weed?", -- action description
          in_money=0, -- money taken per unit
          out_money=300, -- money earned per unit
          reagents={
            ["cannabis"] = 1
          }, -- items taken per unit
          products={ -- items given per unit
          }
        }
      }
    },
    positions = {
      {2570.5222167969,4664.2568359375,34.076766967773},
      {2158.3889160156,4790.029296875,41.122505187988}
    }
  },
  ["cannabisbuyer2"] = {
    def = {
      name="Cannabis Buyer", -- menu name
      permissions = {"gang.drugs"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=2,
      units_per_minute=0.25,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Sell Cannabis"] = { -- action name
          description="I heard you guys got some good link ups for weed?", -- action description
          in_money=0, -- money taken per unit
          out_money=500, -- money earned per unit
          reagents={
            ["cannabis"] = 1
          }, -- items taken per unit
          products={ -- items given per unit
          }
        }
      }
    },
    positions = {
      {1429.2734375,4378.5385742188,44.300201416016},
      {776.0576171875,4183.9008789063,41.780776977539}
    }
  },
  ["meth connect"] = {
    def = {
      name="Cannabis Connection", -- menu name
      permissions = {"gang.drugs"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=500,
      units_per_minute=10,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Collect Meth"] = { -- action name
          description="Collect a kilo of meth from our out of state supplier.", -- action description
          in_money=5000, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["meth"] = 10
          }
        }
      }
    },
    positions = {
      {97.873847961426,3682.3793945313,39.736000061035},
      {48.412605285645,3703.5207519531,39.755027770996},
      {2583.1960449219,4660.6801757813,34.076766967773}
    }
  },
  ["methbuyer1"] = {
    def = {
      name="Meth Buyer", -- menu name
      permissions = {"gang.drugs"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=5,
      units_per_minute=0.1,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Sell Meth"] = { -- action name
          description="The folk in the desert can't get enough of this meth!", -- action description
          in_money=0, -- money taken per unit
          out_money=1250, -- money earned per unit
          reagents={
            ["meth"] = 1
          }, -- items taken per unit
          products={ -- items given per unit
          }
        }
      }
    },
    positions = {
      {749.40606689453,4184.3330078125,41.087852478027},
      {1718.9991455078,4677.150390625,43.655792236328}
    }
  },
  ["methbuyer2"] = {
    def = {
      name="Meth Buyer", -- menu name
      permissions = {"gang.drugs"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=10,
      units_per_minute=0.2,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Sell Meth"] = { -- action name
          description="I need to expand my operation. Can you help me?", -- action description
          in_money=0, -- money taken per unit
          out_money=1000, -- money earned per unit
          reagents={
            ["meth"] = 1
          }, -- items taken per unit
          products={ -- items given per unit
          }
        }
      }
    },
    positions = {
      {-36.492652893066,6580.1630859375,31.426359176636}
    }
  },
  ["police1"] = {
    def = {
      name="Police Report", -- menu name
      permissions = {"police.mission"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=100000,
      units_per_minute=100000,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Write Report"] = { -- action name
          description="Writing Report...", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["police_report"] = 1
          }
        }
      }
    },
    positions = {
      {439.57083129883,-995.072265625,30.689596176147} -- Mission Row
    }
  },
  ["police2"] = {
    def = {
      name="Police Report", -- menu name
      permissions = {"police.mission"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=100000,
      units_per_minute=100000,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Write Report"] = { -- action name
          description="Writing Report...", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["police_report"] = 1
          }
        }
      }
    },
    positions = {
      {1851.6605224609,3690.6713867188,34.267044067383} -- Sandy Shores
    }
  },
  ["police3"] = {
    def = {
      name="Police Report", -- menu name
      permissions = {"police.mission"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=100000,
      units_per_minute=100000,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Write Report"] = { -- action name
          description="Writing Report...", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["police_report"] = 1
          }
        }
      }
    },
    positions = {
      {-449.43395996094,6010.796875,31.716377258301} -- Paleto
    }
  },
  ["ems"] = {
    def = {
      name="Medical Report", -- menu name
      permissions = {"ems.mission"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=100000,
      units_per_minute=100000,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Write Report"] = { -- action name
          description="Writing Report...", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["ems_report"] = 1
          }
        }
      }
    },
    positions = {
      {-272.08700561523,27.639623641968,54.752536773682},
      {465.04064941406,3569.1174316406,33.238555908203},
      {-1145.8566894531,4939.5083007813,222.2686920166}
    }
  }
}

-- time in minutes before hidden transformers are relocated (min is 5 minutes)
cfg.hidden_transformer_duration = 24*60 -- 5 days

-- configure the information reseller (can sell hidden transformers positions)
cfg.informer = {
  infos = {
	["Weapon Black Market"] = 500,
  ["Launderer"] = 250,
  ["cannabis connect"] = 200,
  ["cannabisbuyer1"] = 300,
  ["cannabisbuyer2"] = 300,
  ["meth connect"] = 500,
  ["methbuyer1"] = 400,
  ["methbuyer2"] = 400,
  },
  positions = {
    {1662.4716796875,4819.6611328125,42.046039581299},
    {1696.5936279297,4790.923828125,41.921440124512},
    {1692.5784912109,6431.4936523438,32.763603210449},
    {-414.13354492188,6068.9345703125,31.500135421753},
    {-235.14752197266,6176.4404296875,31.482933044434}
  },
  interval = 60, -- interval in minutes for the reseller respawn
  duration = 15, -- duration in minutes of the spawned reseller
  blipid = 133,
  blipcolor = 2
}

return cfg
