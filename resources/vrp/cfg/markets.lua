
local cfg = {}

-- define market types like garages and weapons
-- _config: blipid, blipcolor, permissions (optional, only users with the permission will have access to the market)

cfg.market_types = {
  ["247"] = {
    _config = {blipid=52, blipcolor=2},

    -- list itemid => price
    -- Drinks
    ["water"] = 5,
    ["milk"] = 6,
    ["icetea"] = 10,
    ["orangejuice"] = 8,
    ["cocacola"] = 10,
    ["redbull"] = 15,
    ["lemonade"] = 10,
    ["vodka"] = 25,

    --Food
    ["bread"] = 4,
    ["donut"] = 5,
    ["sandwich"] = 6,
  },
  ["Fruit Store"] = {
    _config = {blipid=52, blipcolor=2},

    -- list itemid => price
    -- Drinks
    ["forangejuice"] = 5,
    ["smoothie"] = 8,

    --Food
    ["orange"] = 2,
    ["pear"] = 3,
    ["watermelon"] = 5
  },
  ["Restaurant"] = {
    _config = {blipid=52, blipcolor=2},

    -- list itemid => price
    -- Drinks
    ["water"] = 6,
    ["icetea"] = 12,
    ["orangejuice"] = 9,
    ["cocacola"] = 12,
    ["redbull"] = 16,
    ["lemonade"] = 12,

    --Food
    ["fchicken"] = 20,
    ["burrito"] = 18,
    ["tacos"] = 25
  },
  ["Snack Stand"] = {
    _config = {blipid=52, blipcolor=2},

    -- list itemid => price
    -- Drinks
    ["water"] = 5,
    ["cocacola"] = 10,
    ["bcoffee"] = 2,
    ["americano"] = 4,
    ["latte"] = 4,
    ["espresso"] = 2,
    ["tea"] = 4,


    --Food
    ["pdonut"] = 3,
    ["fries"] = 6
  },
  ["drugstore"] = {
   _config = {blipid=51, blipcolor=2},
    ["phanadryl"] = 250,
    ["fakit"] = 300
  },
  ["Tool Store"] = {
    _config = {blipid=402, blipcolor=47},
    ["repairkit"] = 500
  },
  ["Mechanic Store"] = {
    _config = {blipid=402, blipcolor=47, permissions={"repair.market"}},
    ["repairkit"] = 50
  },
  ["Food Truck Warehouse"] = {
    _config = {blipid=478, blipcolor=47, permissions={"ftd.market"}},
    ["tacoshells"] = 2,
    ["mincemeat"] = 5,
    ["mexbeans"] = 1,
    ["tortilla"] = 2,
    ["salsa"] = 1,
    ["spicysalsa"] = 2,
    ["batter"] = 1,
    ["sugar"] = 1,
    ["rawfries"] = 2,
    --Pre Packaged food
    ["burrito"] = 10,
    ["tacos"] = 12,
    ["pdonut"] = 2,
    -- Drinks
    ["water"] = 1,
    ["cocacola"] = 5,
    ["bcoffee"] = 1,
    ["americano"] = 2,
    ["latte"] = 2,
    ["espresso"] = 1,
    ["tea"] = 2
  },
  ["emergencyloadout"] = {
    _config = {blipid=51, blipcolor=68, permissions={"emergency.market"}},
    ["tkit"] = 50,
    ["thaludimol"] = 25
  },
  ["policestore"] = {
    _config = {blipid=51, blipcolor=68, permissions={"police.market"}},
    ["fakit"] = 0,
  },
  ["plantation"] = {
    _config = {blipid=473, blipcolor=4, permissions={"drugseller.market"}},
    ["seeds"] = 500,
	["benzoilmetilecgonina"] = 800,
	["harness"] = 1000
  }
}

-- list of markets {type,x,y,z}

cfg.markets = {
  {"247",-57.094226837158,6523.5600585938,31.490831375122},
  {"247",-93.118186950684,6410.263671875,31.640382766724},
  {"247",161.84913635254,6636.10546875,31.570625305176},
  {"247",1728.8823242188,6414.2001953125,35.037216186523},
  {"247",1698.3356933594,4924.7475585938,42.063678741455},
  {"Restaurant",1590.4891357422,6448.59375,25.317138671875},
  {"Restaurant",2696.9733886719,4325.0478515625,45.85205078125},
  {"Fruit Store",1792.5646972656,4593.6040039063,37.682914733887},
  {"Fruit Store",1087.7102050781,6508.33203125,21.050706863403},
  {"Snack Stand",2741.9443359375,4412.712890625,48.623245239258},
  {"Snack Stand",-271.58416748047,6072.5576171875,31.464773178101},
  {"Snack Stand",153.00402832031,6504.0859375,31.716272354126},
  {"drugstore",150.62184143066,6647.8857421875,31.601047515869},
  {"emergencyloadout",-379.19592285156,6093.0322265625,31.444440841675},
  {"policestore",-247.92950439453,6332.8203125,32.426197052002}, -- Paleto Bay
  {"plantation",1789.86682128906,3896.16943359375,34.3892250061035},
  {"Tool Store",-247.84091186523,6212.8706054688,31.939027786255},
  {"Mechanic Store",1681.1530761719,6428.9770507813,32.17516708374},
  {"Food Truck Warehouse",187.16450500488,6379.0786132813,32.340606689453}
}

return cfg
