local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","FireScript")

-- RESOURCE TUNNEL/PROXY
vrp_firescript = {}
Tunnel.bindInterface("FireScript",vrp_firescript)
Proxy.addInterface("FireScript",vrp_firescript)
vRPbsC = Tunnel.getInterface("FireScript","FireScript")

RegisterServerEvent("FireScript:FirePutOut")
AddEventHandler("FireScript:FirePutOut", function(x, y, z)
	TriggerClientEvent('FireScript:StopFireAtPosition', -1, x, y, z)
end)

RegisterCommand('startfire', function(source, args, rawCommand)
	local user_id = vRP.getUserId({source})
	if vRP.hasPermission({user_id,"firescript"}) then
		TriggerClientEvent('FireScript:StartFireAtPlayer', -1, source, tonumber(args[1]), tonumber(args[2]), args[3] == "true")
		print("starting fire")
	else
		print("Whoa there, you don't have permission for this!")
	end
end, false)

RegisterCommand('stopfire', function(source, args, rawCommand)
	local user_id = vRP.getUserId({source})
	if vRP.hasPermission({user_id,"firescript"}) then
		TriggerClientEvent('FireScript:StopFiresAtPlayer', -1, source)
		print("stopping fire")
	else
		print("Whoa there, you don't have permission for this!")
	end
end, false)

RegisterCommand('stopallfires', function(source, args, rawCommand)
	local user_id = vRP.getUserId({source})
	if vRP.hasPermission({user_id,"firescript"}) then
		TriggerClientEvent('FireScript:StopAllFires', -1)
		print("stopping all fires")
	else
		print("Whoa there, you don't have permission for this!")
	end
end, false)

RegisterCommand('startsmoke', function(source, args, rawCommand)
	local user_id = vRP.getUserId({source})
	if vRP.hasPermission({user_id,"firescript"}) then
		TriggerClientEvent('FireScript:StartSmokeAtPlayer', -1, source, tonumber(args[1]))
		print("starting smoke")
	else
		print("Whoa there, you don't have permission for this!")
	end
end, false)

RegisterCommand('stopsmoke', function(source, args, rawCommand)
	local user_id = vRP.getUserId({source})
	if vRP.hasPermission({user_id,"firescript"}) then
		TriggerClientEvent('FireScript:StopSmokeAtPlayer', -1, source)
		print("stopping smoke")
	else
		print("Whoa there, you don't have permission for this!")
	end
end, false)

RegisterCommand('stopallsmoke', function(source, args, rawCommand)
	local user_id = vRP.getUserId({source})
	if vRP.hasPermission({user_id,"firescript"}) then
		TriggerClientEvent('FireScript:StopAllSmoke', -1)
		print("stopping all smoke")
	else
		print("Whoa there, you don't have permission for this!")
	end
end, false)

print('FireScript by Albo1125 (LUA, C#. FiveM)')
